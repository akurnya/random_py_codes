def mod_exp(a, b, q):
	'''
	Modular Exponentiation like pow(a,b,q) but with 
	http://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method
	'''
    #print (q - 1) * (a % q)
    result = 1
    a = a % q
    while b > 0:
        if (b % 2 == 1):
           result = (result * a) % q
        b >>= 1
        a = (a * a) % q
    return result