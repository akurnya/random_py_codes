import GraphWorld


if __name__ == '__main__':
    import sys
    from string import letters
    import random
    set_vertices = {Vertex(i) for i in letters[0:random.randint(0,len(letters))]}
    GraphWorld.main(Graph({(node1,node2,Edge(node1,node2)) for node1,node2 in combinations({i for i in tuple(set_vertices)[:random.randint(0,len(set_vertices))]},2) if random.randint(0,1)}))