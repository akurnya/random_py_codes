from string import letters,punctuation, digits
container = map(int,digits) + list(letters)

def remove_tag(arg, container):
    out = ""
    tags = False
    assert isinstance(arg, str)
    for num,c in enumerate(arg):
        if num > 0:
            prev,c_status = num-1,c in container
            prev_val = arg[prev]
            if prev == 0:
                if prev_val in container:
                    out += prev_val
            elif c_status and prev_val in punctuation:
                if tags == False:
                    tags = True
                else: tags = False
            if c_status and tags == False:
                out += c
    return out