import sys, math
'''
Statement
In this exercise, you have to analyze records of temperature to find the closest to zero.
 
  
Sample temperatures. Here, -1 is the closest to 0.
 
Write a program that prints the temperature closest to 0 among input data.
 
INPUT:
Line 1: N, the number of temperatures to analyse
Line 2: The N temperatures expressed as integers ranging from -273 to 5526
 
OUTPUT:
Display 0 (zero) if no temperature is provided
Otherwise, display the temperature closest to 0, knowing that if two numbers are equally close to zero, positive integer has to be considered closest to zero (for instance, if the temperatures are -5 to 5, then display 5)
 
CONSTRAINTS:
0 ≤ N < 10000
 
EXAMPLE:
Input
5
1 -2 -8 4 5
Output
1
 
'''
def min_temp():
    # Auto-generated code below aims at helping you parse
    # the standard input according to the problem statement.
    
    N = int(raw_input()) # the number of temperatures to analyse
    if not N: 
        print 0
        return
        #exit()
    
    absolut = min_temp = 0.0
    TEMPS = map(float, (raw_input()).split()) # the N temperatures expressed as integers ranging from -273 to 5526
    
    # Write an action using print
    # To debug: print >> sys.stderr, "Debug messages..."
    if 0 < N < 10000:
        #TEMPS = map(float, TEMPS.split())
        min_temp = min(TEMPS, key = lambda b: abs(b))
        
        absolut = abs(min_temp)
        if absolut in TEMPS and absolut >= 0:
            min_temp = absolut 
        #for num in TEMPS:
        #    if num >= 0 and abs(num) ==  abs(min_temp):
        #        min_temp = num
        
        print int(min_temp)
    return