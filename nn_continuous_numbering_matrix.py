"""
Create an array of arrays of continuous number both sorted and reversed
in a matrix like fashion. A good example where this might be needed?
- Chess
- Chutes & Ladders
"""

def nn_matrix(n):
    nn = (n*n) + 1
    a = []
    last = 1
    for x in xrange(n,nn,n):
        s = x+1
        a += [range(last,s)]
        last = s
    return a


def nn_matrix_reversed(n):
    nn = (n*n)
    a = []
    for x in xrange(nn,0,-n):
        a += [range(x,x-n,-1)]
    return a


def nn_matrix_fast(n):
    nn = (n*n) + 1
    a = []
    l = []
    last = 0
    for x in xrange(1,nn):
        l += [x]
        if x%n == 0:
            a += [l[last:]]
            last = x
    return a


def nn_matrix_reversed_fast(n):
    nn = (n*n)
    a = []
    l = []
    index = 0
    last = nn
    for x in xrange(nn,0,-1):
        l += [x]
        index += 1
        if last-x == n-1:
            a += [l[index-n:]]
            last = x-1
    return a