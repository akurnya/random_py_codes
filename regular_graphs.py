"""
A Netowrk has G has
Nodes - N, 
Edges/Connections = e,
Degree of Node on an average - deg
distance - shortest path/navigation;
diameter - D(u,v):  the average pairwise shortest path distance between vertices; u,v: Nodes/Vertices
Avg. Diameter = Summation(D(u[i],v[i]))/[N(N-1)/2] - Total regular connected nodes of Network N

if C(G) >> p : High CLustering of Network G. PERIOD.

Here's a good question:
Suppose we are generating a network of 16 vertices in the alpha model with alpha = 1 and default probability p = 1/2. Further, suppose that two vertices u and v have 4 neighbors in common. If we use the formula y ~ p + (x/N)^alpha, as discussed in the lecture, then the probability of adding the u-v edge is proportional to:[x: num of common neighbours]
"""

import gevent
from gevent import monkey
monkey.patch_all()
from math import sqrt, ceil, floor
import pprint
from itertools import combinations
from operator import add
from collections import deque, defaultdict
import memoization
from memoization import memoize_uw
r = float
intr = int


#@memoize_uw
class Directed_Graph(Regular_Graph):
    def __init__(self, **attr):
        super(type(self),self).__init__(**attr)
    def directed_graph(self):
        return self.nodes*r(self.nodes-1)


#@memoize_uw
#Growth Rate Theta(n^2):Quadratic
class Regular_Graph(object):
    def __init__(self, **attr):
        if 'nodes' in attr:
            self.nodes = attr['nodes']
        else:
            self.nodes = None
        if 'edges' in attr:
            self.edges = attr['edges']
        else:
            self.edges = None
        if 'deg' in attr:
            self.deg = attr['deg']
        else:
            self.deg = None
    
    """
    Recursive Model
    Problem: Runtime Error: Maximum Recursion Depth exceeds
    if num != 1:
        return num -1 + regular_connected_graph(num -1) 
    else:
        return 0
    #For larger num a better model, w/o recursion exceed error
    ret, prev, r = 0, 1, num-1
    if num > 2:
        ret = prev
        while prev < r:
            prev += 1
            ret += (prev)
        return (ret)
    elif num == 2:
        return 1
    else:
        return 0
    #After applying Generalizationa and asymptotics:this def comes up
    """
    #Find all connections=edges=e (UNDIRECTED)
    def regular_connected_graph(self,deg = None): return self.nodes*r(self.nodes-1)/(2) if edge is None else self.nodes*r(deg)/(2)

    
    def chain_graph_edges(self): return self.nodes-1
    
    """
    Diameter OR average shortest path of regular Cycle
    as in Regular Cycle => Nodes = edges = Ring Network;
    When its a Cycle of Nodes with Degree = 2 and e Edges = N Nodes:
    How am I doing the following?:
    count edge distance d(u,v) from u to any random v.
    Do it per node and since in doing so we count each node twice, divide it by 2.
    Hence for N=3,edge = 3; N*2/2 = 3
    and avg distance /diameter (In Professor Michael Kearns sense): 3 *2/N(N-1) = 1 
    N = 3,e=3, N*2/2 n-1
    N=4,e=4, N*4/2 = 8 n
    
    N=5,e=5, N*6/2 = 15 * 2/5,4 n+1
    
    N =6,e=6, N*8/2 = 24 * 2/6,5 = 8/2 n+2
    N=7,e=7,n+3
    N = 12, 12-4 = 8, n+8
    n+3/n+2, n+1/n, n/n-1;
    
    Generalization of Summation(All diameter(u,v):
    N*(N+N-4)/2 = N*(2N-4)/2 = N*(N-2);
    N*(N-2)*2/N*(N-1) = 2*(N-2)/(N-1)
    """
    def diameter_avg_shortest_path_regular_cycle_polygon(self,deg = 2): return 2*r(self.nodes-2)/(self.nodes-1) if self.nodes>deg else None
    
    #When connections=edges=e are given and d=N-1; and N-nodes is unknown;Find N
    #Connected as undirected 3D graph nodes
    def inverse_find_nodes_max_degree(self,*deg):
        """ 
        When max degree given, solve e = n(n-1)/2 by making it into a Quadratic Expression. And solve it for n! SO E G!
        #when deg = N-1 assumed = maximum edges are connected as max e = deg*N/2 = N(N-1)/2
        """
        return abs(-1-sqrt(1+(8*self.edges)))/r(2) if not deg else ceil(abs(-1-sqrt(1+(8*self.edges)))/r(2))
        #N = connections + 1 + regular_connected_graph(N-1)
    
    
    #When Nodes N and edges e are given, find avg degree per Node.
    def avg_degree(self): #Works best for a regular RING graph
        return 2*(self.edges)/r(self.nodes)
    
    def max_degree(self): return self.nodes-1
    
    
    def CC(self, num_conn_toN,e_of_conn_neighours): return 2*r(e_of_conn_neighours)/(num_conn_toN*(num_conn_toN-1))
    
    def edge_density(self): return r(self.edges)/(self.nodes*r(self.nodes-1)/2)
    

class Graph_Grid(object):
    def grid_network_graph(N):
        """
        Needs improvization 
        to run isprime module to test division by all primes upto
        sqrt(N). Whichever gives perfect x,y is the ans
        """
        x = N/r(floor(sqrt(N)));y = N/r(x)
        if intr(x) == x and intr(y) == y:
            return (x*(y-1))+(y*(x-1))
        elif N%2 == 0:
            x = N/r(2)
            return x+(2*(x-1))
        elif N%3 == 0:
            x = N/r(3)
            return (x*(2))+(3*(x-1))
        elif N%5 == 0:
            x = N/r(5)
            return (x*(4))+(5*(x-1))
        else:
            return None



class Vertex(tuple):
    """A Vertex is a node in a graph."""

    def __init__(self, label):
        super(type(self),self).__init__()
        self.label = (label)

    def __repr__(self):
        """Returns a string representation of this object that can
        be evaluated as a Python expression."""
        return 'Vertex(%s)' % repr(self.label)

    def __eq__(self,other):
        if isinstance(other, Vertex):
            if self.label == other.label:
                return True
            else: return False

    __str__ = __repr__
    """The str and repr forms of this object are the same."""


class Edge(tuple):
    """An Edge is a list of two vertices."""

    def __new__(cls, *vs):
        """The Edge constructor takes two vertices."""
        if len(vs) != 2:
            raise ValueError, 'Edges must connect exactly two vertices.'
        return tuple.__new__(cls, vs)
    
    def __eq__(self,other):
        if isinstance(other, Edge):
            if self[0] in other and self[1] in other:
                return True
            else: return False

    def __repr__(self):
        """Return a string representation of this object that can
        be evaluated as a Python expression."""
        return 'Edge(%s, %s)' % (repr(self[0]), repr(self[1]))

    __str__ = __repr__
    """The str and repr forms of this object are the same."""


class Graph(object):
    """A Graph is a dictionary of dictionaries.  The outer
    dictionary maps from a vertex to an inner dictionary.
    The inner dictionary maps from other vertices to edges.

    For vertices a and b, graph[a][b] maps
    to the edge that connects a->b, if it exists."""

    def __init__(self, v): #vs = [(v,w)], v= single Vertex()
        """Creates a new graph.  
        vs: list of vertices;
        es: list of edges.
        """
        self.graph_dict = {}
        #self.make_link(vs)
        self.make_vertex(v)
        self.N = len(self)
        self.Nedges = len(self.edges())
        self.Edges = None
    
    
    def __str__(self):
        pp = pprint.PrettyPrinter()
        return pp.pformat(self.graph_dict)
    
    def __getitem__(self, key): return self.graph_dict.get(key)
    
    def __setitem__(self,key,val): self.graph_dict[key] = val
    
    def __contains__(self,key): return key in self.graph_dict
    
    def __iter__(self):
        for key in self.graph_dict:
            yield key
    
    def __len__(self):
        self.__len__ = self.graph_dict.__len__
        return self.__len__()
    
    
    
    def make_vertex(self, L):
        for x in L:
            self.add_vertex(x)


    def make_link(self, vs, add_edge_status = None):  #vs = [(v,w)]
        for v in vs:
            if len(v)==2 and isinstance(v, tuple):
                #node1,node2 = v[:2]
                for node in v:
                    self.add_vertex(node)
                if add_edge_status == 'ON':
                    self.add_edge(Edge(v[0],v[1]))



    def get_edge(self, v, u):
        try:
            return self[v][u]
        except (KeyError,NameError) as e:
            print "No such edge exists"
            return None

    def remove_edge(self,v,u):
        try:
            del self.graph_dict[v][u],self.graph_dict[u][v]
        except (KeyError,NameError) as e:
            print "No such edge exists \n",
            print e.args
            return None


    def vertices(self):  #Total Vertices
        return self.graph_dict.keys()

    def edges(self):  #Total Edges
        return tuple(set([edge for node,edge in reduce(add, [node_edge.items() for node_edge in self.graph_dict.values()])]))

    def out_vertices(self,v):
        """Vertices connected to a node"""
        return self[v].keys() if self[v] is not None else None

    def out_edges(self,v):
        """Edges connected to a node"""
        return self[v].values() if self[v] is not None else None


    def add_vertex(self, v):
        """Add a vertex to the graph."""
        if v not in self:
            self[v] = {}

    def add_edge(self, e):  #e = Edge(node1,node2)
        """Adds and edge to the graph by adding an entry in both directions.

        If there is already an edge connecting these Vertices, the
        new edge replaces it.
        """
        v, w = e
        if v in self and w in self and v != w:
            self[v][w] = e  #self.graph_dict[v][w] = e
            self[w][v] = e  #self.graph_dict[w][v] = e
        else:
            print "No such Nodes/Vertices present in this Graph. You might first wana add those Vertices Orr Maybe you wanted to connect a node to itself\n"
            return None

    def add_all_edges(self):
        #node_edge_list = []
        total_edges = self.regular_connected_graph()
        if total_edges != self.Nedges:
            """
            for it in combinations(self.graph_dict.keys(),2):
                if self.get_edge(it[0],it[-1]) is None:
                    new_edge = Edge(it[0],it[-1])
                    node_edge_list.append((it[0],it[-1],new_edge))
            """
            node_edge_list = [self.add_edge(Edge(it[0],it[-1])) for it in combinations(self.graph_dict.keys(),2) if self.get_edge(it[0],it[-1]) is None]
            #self.make_link(node_edge_list)
            self.N = len(self)
            if len(self.edges()) == total_edges:
                self.Nedges = total_edges
            else:
                print self.Nedges,
                print len(self.edges())
                print total_edges
                raise Exception("self.Nedges IS NOT EQUAL TO total_edges. Check this function")


    def regular_connected_graph(self,deg = None):
        return self.N*r(self.N-1)/(2) if deg is None else self.N*r(deg)/(2)

    def is_connected(self):
        """
        Based on True Story: BFS (Breadth First Search)
        The order of growth for BFS is O(|V| + |E|)
        """
        queued_nodes = {nodes: len(self.out_vertices(nodes)) for nodes in self}
        return True if sum(queued_nodes.values()) == self.regular_connected_graph() else False


    def chain_graph_edges(self): return self.N-1


    def diameter_avg_shortest_path_regular_cycle_polygon(self,edges = 1):
        """
        Diameter OR average shortest path of regular Cycle
        as in Regular Cycle => Nodes = edges = Ring Network;
        When its a Cycle of Nodes with Degree = 2 and e Edges = N Nodes:
        How am I doing the following?:
        count edge distance d(u,v) from u to any random v.
        Do it per node and since in doing so we count each node twice, divide it by 2.
        Hence for N=3,edge = 3; N*2/2 = 3
        and avg distance /diameter (In Professor Michael Kearns sense): 3 *2/N(N-1) = 1 
        N = 3,e=3, N*2/2 n-1
        N=4,e=4, N*4/2 = 8 n

        N=5,e=5, N*6/2 = 15 * 2/5,4 n+1

        N =6,e=6, N*8/2 = 24 * 2/6,5 = 8/2 n+2
        N=7,e=7,n+3
        N = 12, 12-4 = 8, n+8
        n+3/n+2, n+1/n, n/n-1;

        Generalization of Summation(All diameter(u,v):
        N*(N+N-4)/2 = N*(2N-4)/2 = N*(N-2);
        N*(N-2)*2/N*(N-1) = 2*(N-2)/(N-1)
        """
        return 2*r(self.N-2)/(self.N-1) if self.N>edges else None

    #When connections=edges=e are given and d=N-1; and N-nodes is unknown;Find N
    #Connected as undirected 3D graph nodes
    def inverse_find_nodes_max_degree(self,*deg):
        """ 
        When max degree given, solve e = n(n-1)/2 by making it into a Quadratic Expression. And solve it for n! SO E G!
        #when deg = N-1 assumed = maximum edges are connected as max e = deg*N/2 = N(N-1)/2
        """
        return abs(-1-sqrt(1+(8*self.Nedges)))/r(2) if not deg else ceil(abs(-1-sqrt(1+(8*self.Nedges)))/r(2))
        #N = connections + 1 + regular_connected_graph(N-1)


    #When Nodes N and edges e are given, find avg degree per Node.
    def avg_degree(self): #Works best for a regular RING graph
        return floor(2*(self.Nedges)/r(self.N))

    def max_degree(self): return self.N-1

    def CC(self, Node):
        def CC_Val(num_conn_toN,e_of_conn_neighours):
            """
            How many edges connected around a Node out of Total number of possible Neighbour to Neighbour Node connections around that Node.
            Clustering Coefficient c(u) = #of Actual edge connections e among neighbours of that node is:
            u /n(n-1)/2;
            u = Number of edges between neighbours
    e = #of Actual edge connections; n = num of connections to that Node
            """
            return 2*r(e_of_conn_neighours)/(num_conn_toN*(num_conn_toN-1))
        neighbours = len(self.out_vertices(Node))
        if Node in self:
            e_of_conn_neighours = len({N for N in (self.get_edge(n1,n2) for n1,n2 in combinations((node for node in self.out_vertices(Node)),2)) if isinstance(N,Edge)})
            print "neighbours %s" %(neighbours)
            print "\ne_of_conn_neighours {}".format(e_of_conn_neighours)
            return CC_Val(neighbours, e_of_conn_neighours) if e_of_conn_neighours != 0 else 0


    def CC_Graph_avg(self):
        """Avg Cluster Coeff of Graph G C(G) = Avg(c(u)/N(Total Vertices/Nodes in G)"""
        return sum((self.CC(node) for node in self))/len(self)
    
    
    def find_path(self,v,w):
        """
        def mark_component(G, node, marked):
            pipeline = set()
            marked[node] = True
            total_marked = 1
            pipeline.add(node)
            while len(pipeline) != 0:
                item = pipeline.pop()
                for neighbor in G[item]:
                    if neighbor not in marked:
                        marked[neighbor] = True
                        pipeline.add(neighbor)
                        total_marked += 1  #mark_component(G, neighbor, marked)
                    print total_marked
            return total_marked
        """
        """
        def is_path(v, w, covered = None, path = None):
            #Check if the two nodes are in a single component and make path. Not necessarily in one hop but across many edges
            covered = covered or set()
            path = path or {}
            is_path.result = is_path.result or None
            if len(self[v]) == 0:
                return None
            edge = self.get_edge(v, w)
            if isinstance(edge, Edge):
                return w
            elif edge is None:
                if v not in covered:
                    covered.add(v)
                    for nodes in self[v]:
                        if path.get(v) is None:
                            path[v] = is_path(nodes, w, covered, path)
                        else:
                            path[v] = (path[v],is_path(nodes, w, covered, path))
                    is_path.result = path
                else:
                    return path[v]
        """
        if len(self) >=1:
            def is_path(start, end, covered = None, path = None):
                """Check if the two nodes are in a single component and make path. Not necessarily in one hop but across many edges"""
                covered = covered or set()
                path = path or defaultdict(set)
                is_path.result = is_path.result or None
                if len(self[start]) == 0:
                    is_path.result = None
                    return is_path.result
                edge = self.get_edge(start, end)
                if isinstance(edge, Edge):
                    is_path.result = path[start].add(end)
                    return is_path.result
                elif edge is None:
                    #BFS starts here
                    covered.add(start)
                    x = self.out_vertices(start)
                    is_path.inpipeline.extend(x)
                    path[start].update(x)
                    while len(is_path.inpipeline) != 0:
                        #BFS exploration on all nodes connected to a node starts
                        pop_node = is_path.inpipeline.popleft()
                        if pop_node not in covered:
                            covered.add(pop_node)
                            for node in self[pop_node]:
                                if node not in is_path.inpipeline and node != start:
                                    is_path.inpipeline.append(node)
                                    path[pop_node].add(node)
                is_path.result = path
            def recur_find_path(graph, start, end, path=[]):
                if start == end:
                    return path + [end]
                if not graph.has_key(start):
                    return None
                path = path + [start]
                for node in graph[start]:
                    if node not in path:
                        newpath = recur_find_path(graph, node, end, path)
                        if newpath: recur_find_path.final_path.append(newpath)
                return None
            is_path.inpipeline = deque()
            is_path.result = None
            recur_find_path.final_path = []
            is_path(v, w)
            if is_path.result is None:
                return None
            recur_find_path(is_path.result, v, w)
            print is_path.result
            return recur_find_path.final_path
        else: return None

    def edge_density(self): 
        """edge density p = Actual #edges connected total E/ [N(N-1)/2]"""
        return r(self.Nedges)/(self.N*r(self.N-1)/2)


class Planar_Graph(Graph):
    def __init__(self, vs, **attr):
        super(type(self),self).__init__(vs)
        self._nmr(**attr)


    def _nmr(self, **attr): #nodes,edges,regions : EULER UNCLE's RULE
        if 'region' in attr:
            self.region = attr['region']
            print self.region
            if self.N - self.Nedges + self.region == 2:
                pass
            else:
                raise Exception("Not a planar graph")
        elif self.N and self.Nedges:
            setattr(self,'region',2+self.Nedges - self.N)
            print self.region
        elif self.N and 'Edges' in attr:
            self.Edges = attr['Edges']
            setattr(self,'region',2+self.Edges - self.N)
            print self.region
    
    def growth_rate(self): return 3*r(self.N) - 6 #edges <=3*nodes - 6 Grows linearly
    

#Commit - This I am still working    
class Combo_Lock_Graph(Graph):
    def __init__(self,vs,**attr):  #vs = [(v,w)]
        super(type(self),self).__init__(vs)
        self._create_combo_lock()
        self.N = len(self)
    
    def Combo_Length(self):
        return 2*len(self.edges())-3 


    def _create_combo_lock(self):
        #G = {}  #For Graph Not Giraffe
        nodes = tuple(self.graph_dict.keys())
        #nodes = tuple(sorted(self.graph_dict.keys(), key=lambda X: X.label))
        start = nodes[0]
        for enum, N in enumerate(nodes):
            try:
                self.add_edge(Edge(N,nodes[enum+1]))
                #make_link(G,N,nodes[enum+1])
            except (KeyError,IndexError) as e:
                print (e.args),
                print "\nAll nodes added to link, reached exception stage\n"
            if N != start and N not in self[start] or start not in self[N]:
                self.add_edge(Edge(start,N))
        #return G
        print self
        print "seems like we reached create combo lock w/o errors"



"""
if __name__ == "__main__":
    import sys
    num = int(sys.argv[1])
    print regular_connected_graph(num)
"""