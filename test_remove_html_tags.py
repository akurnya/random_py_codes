import unittest
from remove_html_tags import *

"""
def remove_tag(arg, container):
    out = ""
    tags = False
    assert isinstance(arg, str)
    for num,c in enumerate(arg):
        if num > 0:
            prev,c_status = num-1,c in container
            prev_val = arg[prev]
            if prev == 0:
                if prev_val in container:
                    out += prev_val
            elif c_status and prev_val in punctuation:
                if tags == False:
                    tags = True
                else: tags = False
            if c_status and tags == False:
                out += c
    return out
"""


class test_remove_tags(unittest.TestCase):
    #feature by feature tests
    def setUp(self):
        self.sample1 = 'xyz'
        self.sample2 = '"<b>foo</b>"'
        self.sample3 = "'<b>foo</b>'"
    
    def test_arg_is_string(self):
        for i in [self.sample1,self.sample2,self.sample3]:
            self.assertIsInstance(i, str)
            return True
    
    def test_func_pass(self):
        self.assertEqual(remove_tag(self.sample1), 'xyz')
        self.assertEqual(remove_tag(self.sample2), 'foo')
        self.assertEqual(remove_tag(self.sample1), 'foo')


if __name__ == "__main__":
    unittest.main()