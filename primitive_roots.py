'''
Based on 
Udacity Cryptography PS3-2 Problem passes the followig solution written step by step as described here:
http://math.stackexchange.com/questions/124408/finding-a-primitive-root-of-a-prime-number
'''

from math import sqrt

def gcd(num,den):
    if num<den: num,den=den,num
    rem = num%den
    #print num,den,rem
    while rem > 0:
        num,den = den,rem
        rem=num%den
        #print num,den,rem
    return den == 1


def fact(n):
    '''
    factorial of a number n
    '''
    i = n
    if n <= 1: return 1
    i = n - 1
    while i > 1:
        
        n = n * i
        i -= 1
    return n


def fp(p):

    """

    AKS Primalty Test: (x-1)**p - (x**p-1)

    #multiply nCk = fact(n)/fact(n-k)*fact(k) AND (-1)**k 

    #from binomial expansion

    if fp(p): p is Prime Number

    """
    def _coeff_add(i):
        res = ((-1)**i)*factp/(fact(p-i)*fact(i))
        #if i != 0 and i != p: return res
        if i==0: return 0
        if i==p: return res + 1
        return res
    factp = fact(p)
    return not any([_coeff_add(i)%p!=0 for i in xrange(p)])


def prime_factorization(p,s):
    '''
    p: prime number
    s: Euclied's totient function phi(p) = p-1
    '''
    s_int = 0
    n = s
    factors = []
    i = 2
    while i < n:
        if fp(i):  #is prime
            while 1:
                s_int = s/i
                if s - i*s_int: break;
                if s_int == 1: 
                    factors.append(p/i)
                    return set(factors)
                s = s_int
                factors.append(p/i)
        i += 1
    return set(factors)

def find_primitive_roots(p):
    if p == 3: return [2]
    m_exp = 2
    tried = []
    primitive = []
    s = p-1
    factors = list(prime_factorization(p,s))
    if factors:
        for i in xrange(2,s+1):
            if sqrt(i) not in tried \
            and i not in tried:
                if all([pow(i,exp,p) != 1 for exp in factors]):
                    primitive.append(i)
                    #Compare i's powers if they satisfy critieria
                    '''
                    Need to work on this issue
                    while m_exp < p:
                        num = i**m_exp
                        if num >= p:
                            break;
                        if gcd(m_exp,s): # and pow(i,m_exp,p) != 1:
                            tried.append(num)
                            primitive.append(num)
                        m_exp += 1
                    m_exp = 2
                    '''
                    tried.append(i)
    del tried
    return primitive


def primitive_roots(n):
    """Returns all the primitive_roots of 'n'"""
    return find_primitive_roots(n)


def test():
    assert primitive_roots(3) == [2]
    assert primitive_roots(5) == [2, 3]
    print "tests pass"

test()