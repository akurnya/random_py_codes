def gcd(D,d):
    res = d
    old_Ds, Ds = 1,0
    old_dt, dt = 0,1
    r = D%d
    while r != 0:
        q = D/d
        #Bézout's identity
        Ds, old_Ds = (old_Ds - (q* Ds)), Ds
        dt, old_dt = (old_dt - (q* dt)), dt
        D,d = d,r
        r = D%d
    print d, Ds, dt
    '''(Ds + dt(x))*D % d = 1 | x={-inf..0..+inf}
    if (Dx cong= 1) mod d then x is inverse and
    x should be POSITIVE
    '''
    while Ds < 0:
        Ds += res
    return d, Ds, dt