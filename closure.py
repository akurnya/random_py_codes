"""
MORAL OF THE STORY: WHEN YIED GENERATOR CAN BE USED INSTEAD OF DECORATORS, USE
IT UNLESS YOU REALLY WANT TO HIDE THE VARIABLE
"""

def count_parity(numbers):
    """Great thing about generators is that its not a CLOSURE
    which means each function assigned to an object variable is totally a 
    new instance type hence independent of each other"""
    even_count = odd_count = 0
    for number in numbers:
        if number % 2 == 0:
            even_count += 1
        else:
            odd_count += 1
        yield even_count, odd_count
