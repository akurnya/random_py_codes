#Dave's Office of PyCon 2013
#Python 2.7

import urllib, webbrowser, math, datetime, time
from xml.etree.ElementTree import parse, dump

buses={}
previous_busid={}
lat0= 41.980262
longitude0= -87.668452


u=urllib.urlopen('http://ctabustracker.com/bustime/map/getBusesForRoute.jsp?route=22')
data=u.read()
f=open('rt22.xml','wb') #C:\Users\Hero\Desktop\my_pys\\
f.write(data)
f.close()
doc=parse('rt22.xml') #C:\Users\Hero\Desktop\my_pys\\
#dump(doc)
tempdoc=doc

def bus_in_xml_doc(doc):
    for bus in doc.findall('bus'):
        #now=datetime.datetime.now().strftime("%H%M")
        direction=str(bus.findtext('d'))
        dd=bus.findtext('dd')
        driver=bus.findtext('fs')
        busid=bus.findtext('id')
        lat=float(bus.findtext('lat'))
        lon=float(bus.findtext('lon'))
        #print '%s hrs' %(now)
    yield direction,dd,driver,busid,lat,lon
    
#def track_bus(): #requires doc=parse('rt22.xml')
for direction,dd,driver,busid,lat,lon in bus_in_xml_doc(doc):
    #if dd=='Northbound':
    buses.setdefault('direction',[]).append(direction) #buses.append((busid,dd,driver))
    buses.setdefault('dd',[]).append(dd)
    buses.setdefault('driver',[]).append(driver)
    buses.setdefault('busid',[]).append(busid)
    buses.setdefault('lat',[]).append(lat)
    buses.setdefault('lon',[]).append(lon)
#buses=tuple(buses)
#print buses
    
def dist_latlong(lat0,longitude0,lat1,longitude1):
    a=(math.sin(float(lat1-lat0)/2))**2 + math.cos(float(lat0))*math.cos(float(lat1))*((math.sin(float(longitude1-longitude0)/2))**2)
    d=2*6371*(math.asin(math.sqrt(a)))
    return d

def monitor():
    global tempdoc
    global buses #global dict()
    global previous_busid #global dict()
    print buses
    u=urllib.urlopen('http://ctabustracker.com/bustime/map/getBusesForRoute.jsp?route=22')
    #data=u.read()
    doc=parse(u)
    for direction,dd,driver,busid,lat,lon in bus_in_xml_doc(doc):
        if buses.has_key(busid): #Just to cross check if buses got busid key and its corresponding values from above
            if busid in buses[busid]: # and lat>lat0: # and if lat>lat0:
                d=math.fabs(dist_latlong(lat0,longitude0,lat,lon))
                if busid in previous_busid:
                    if cmp(long(float(previous_busid[busid])),d)>=0:
                        last_dist_diff=math.fabs(float(previous_busid[busid])-d)
                        previous_busid[busid]=str(d)
                        if last_dist_diff==0:
                            status='%s distance away from you and hasn\'t moved away from' %(d)
                        else:
                            status='%s distance away from you and moved away by %s distance from' %(d,last_dist_diff)
                    else:
                        last_dist_diff=math.fabs(d-long(float(previous_busid[busid])))
                        previous_busid[busid]=str(d)
                        status='%s distance towards you and moved closer by %s distance since last time from' %(d,last_dist_diff)
                        #status='%s distance towards you and came %s distance closer to' %(d,last_dist_diff)
                else:
                    previous_busid[busid]=str(d) #previous_busid.setdefault(busid,[])=str(d)
                    status='within range of %s distance and hasn\'t moved from last time' %(d)
                #if d<=1:
                print 'Your bus is %s with id %s and driver %s and is %s you.Took Xs time for the bus to come in your range' %(direction,busid,driver,status) #,datetime.datetime.now().strftime("%H%M")-now)
    tempdoc=doc

if __name__ == "__main__":
    while True:
        try:
            monitor()
            time.sleep(5)
        except KeyboardInterrupt,e:
            f=open('rt22_backup.xml','wb') #C:\Users\Hero\Desktop\my_pys\\
            xml_dump=str((tempdoc))
            f.write(xml_dump)
            f.close()
            dump(tempdoc)
            raise e
            print buses #,'last busid tracked: %s' %(busid)
            buses.clear()
            previous_busid.clear()
            break