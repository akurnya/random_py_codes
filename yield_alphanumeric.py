"""
Write a generator that yields an infinite sequence of alpha-numeric identifiers, starting
with a1 through z1, then a2 through z2, and so on.
"""

def iteralphanum():
    let = letters[:26]
    while 1:
        for num in xrange(1,len(let)):
            for enum,i in enumerate(let):
                yield (i)+str(num)
                

it = iteralphanum()