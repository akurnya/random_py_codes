def fact(n):
    '''
    factorial of a number n
    '''
    i = n
    if n <= 1: return 1
    i = n - 1
    while i > 1:
        
        n = n * i
        i -= 1
    return n


def fp(p):

    """

    AKS Primalty Test: (x-1)**p - (x**p-1)

    #multiply nCk = fact(n)/fact(n-k)*fact(k) AND (-1)**k 

    #from binomial expansion

    if fp(p): p is Prime Number

    """
    def _coeff_add(i):
        res = ((-1)**i)*factp/(fact(p-i)*fact(i))
        #if i != 0 and i != p: return res
        if i==0: return 0
        if i==p: return res + 1
        return res
    factp = fact(p)
    return not any([_coeff_add(i)%p!=0 for i in xrange(p)])

def prime_factors(s):
    '''
    s: prime number factorization
    '''
    s_int = 0
    n = s
    factors = []
    i = 2
    while i < n:
        if fp(i):  #is prime
            while 1:
                s_int = s/i
                if s - i*s_int: break;
                if s_int == 1: 
                    factors.append(i)
                    return set(factors)
                s = s_int
                factors.append(i)
        i += 1
    return set(factors)