def percentile(data, k_per, upper = 1):
    ln = len(data)
    rem = int(k_per/100. * ln)
    return data[:ln - rem] if upper else data[rem:]