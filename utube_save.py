import os, subprocess, re, cStringIO

rg = re.compile('//www.youtube.com/watch?'+'.*?v='+'([a-zA-Z0-9_]+)',re.IGNORECASE|re.DOTALL)

p = ""  #r"/home/paul/paul_private/"
"""
if os.path.abspath((os.getcwd())) != p:
    os.chdir(os.path.abspath(p))
"""
def save_vid(p=""):
    f = cStringIO.StringIO()
    if os.path.isfile(p+r'//'+r"links.txt") and p != "":
        with open(p+r'//'+r'links.txt', 'rw+') as fin:
            for line in fin.xreadlines():
                trun= fin.tell()
                #print trun
                d = None
                #print line
                l = [i.strip() for i in line.split(':')]
                print l
                if len(l) == 3:
                    s = l[1]
                elif len(l) == 2:
                    s = l[0]
                d = rg.search(s).group(1)
                if 'NO' in l and d is not None:
                    print 'downloading {0}'.format(d)
                    outp = subprocess.Popen('youtube-dl -t '+ d, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                    msg =  outp.communicate()[0]
                    #print msg
                    #print len(msg)
                    if msg is not None:
                        #fin.seek(fin.tell()-trun)
                        #fin.truncate()
                        #print fin.tell()
                        l[l.index('NO')] = 'YES'
                        f.writelines(':'.join(l))
                        f.writelines('\n')
                        #fin.writelines(':'.join(l))
                        #fin.seek(trun)
            fin.close()
        content = f.getvalue()
        f.close()
        with open(p+r'links.txt', 'w+') as fin:
            for line in content:
                #print line
                fin.writelines(line)
            fin.close()
    return

if __name__ == "__main__":
    save_vid()