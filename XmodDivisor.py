#XmodDivisor.py
"""
#X % Y = rem
#Thanks Samuel Chukwuemeka
Eg: 6 mod 5 = 1
11 mod 5 = 1
..
X mod 5 = 1 ? Thats the main problem.

i.e. 
X = {5*(n=1....<=X-1) + 1}
Form: X mod Y = rem

Another Eg:
g = mod_gets_res(5,2,neg=True,res=-9)

g.next()

g.next()

g.next()
Out[43]: (-9, -3)

g.next()
---------------------------------------------------------------------------
StopIteration                             Traceback (most recent call last)
<ipython-input-44-d7e53364a9a7> in <module>()
----> 1 g.next()

StopIteration: 

"""

def mod_gets_dividends(Y,rem,res=None,neg=False):
    #Y(q) + rem
    #Y*(-q) + rem  #-q -= 1
    #def _op(q): return q+1 if op else q-1
    if rem != abs(rem): raise "Remainder cannot be negative"
    if rem and res: 
        rem_diff = res - (Y * (res/Y))
        if res/Y < rem:
            raise Exception("rem_diff {} > rem {} i.e. input Dividend is small enough to have that remainder".format(rem_diff,rem))
    temp,count = None, 1
    if not neg: X,rem = Y + rem,Y #q=op=1
    else: X,rem = -Y + rem,-Y  #op,q = 0,-1 
    while 1:
        if res:
            if X == res:
                yield X, res, count  #,q
                break;
            if not neg and X > res:
                break;
            if neg and X <= res:
                yield X, res, -count  #,q
                break;
            else: 
                temp,X = X,None
        yield X, count
        if temp:
            X,temp = temp,None
        #q = _op(q)
        X += rem
        count += 1

#Mod Addition
def mod_add(N,num1,num2): return (num1+num2) % N

#Mod Multiplication
def mod_mul(N,num1,num2): return (num1*num2) % N