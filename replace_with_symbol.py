from itertools import combinations_with_replacement

def replace_with_symbol(String, symbol = '?', comb_num = (0,1)):
    def replace(s,Cnext): return str(Cnext.next()) if s==symbol else s
    String_list = list(String)
    sym_index = filter(lambda X: X==symbol, String_list)  #[index for index,i in enumerate(String) if i==symbol]
    comb = combinations_with_replacement(comb_num,len(sym_index))
    for c in (comb):
        #replace the with next of c
        Cnext = (i for i in c)
        print ''.join([replace(s,Cnext)
        for s in String_list])