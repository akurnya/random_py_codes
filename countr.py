"""
You are given an integer m such that (1 ¬ m ¬ 1 000 000) and a non-empty, zeroindexed
array A of n integers: a0, a1, . . . , an−1 such that (0 ¬ ai ¬ m). Count the number of
occurrences of the values 0, 1, . . . ,m.
"""


def countr(Array, dynamic = False):
    """
    iff for all m in Array; m < len(Array)
    But if you dynamically want to store for all m s.t. any m > len(Array)
    provided the length is a short list.
    Use it in case you already know the list is not that long
    but has high variance.
    """
    if dynamic: N = max(Array)
    else: N = len(Array)
    arr = [0] * N
    for num in Array:
        arr[num] += 1
    return arr