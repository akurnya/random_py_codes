def sec_count(sumsec):
    if sumsec >= 60:
        temp = float(sumsec) / 60
        minute = sumsec / 60
        sec = (temp - minute)*60
        print 'temp:{} minute:{} sec:{}'.format(temp, minute, sec)
        x = sec_count(sec)
        if isinstance(x, tuple):
            finalsec = x[1]
            minute += x[0]
        else:
            finalsec = x
        #tot_minutes = int(finalsec)
        #minute += (finalsec - tot_minutes)
        #numstr = str(finalsec)
        #print 'numstr', numstr
        #sumsec = float("{0:.2f}".format(int(numstr[numstr.find('.')+1:])))
        #return minute, sumsec #temp0 - before dot, temp1 - after dot
        return minute, finalsec
    else:
        return sumsec #sumsec or final are seconds

def add_time(t1, t2):
    sum = Time()
    sum.hour = t1.hour + t2.hour
    sum.minute = t1.minute + t2.minute
    sum.second = t1.second + t2.second
    stime = sec_count(sum.second)
    if isinstance(stime, tuple):
        minute, second = stime
        sum.minute += minute
    else:
        second = stime
    sum.second += second
    mtime = sec_count(sum.minute)
    if isinstance(mtime, tuple):
        hour, minute = mtime
        sum.hour += hour
    else:
        minute = mtime
    sum.minute += minute
    return sum