
def sqr(x,branch = 0):
    while isinstance(x,int) and x*x != 16:
        #print branch
        branch += 1
        x = sqr(x+1, branch)
    return x

if __name__ == "__main__":
    import sys
    i = int
    print sqr(i(sys.argv[1]))

