import os, re
from itertools import imap


def search_files_under_dir(regex, path):   
    '''
    walk through a path.
    search for a regex file in bunch of all files in that path
    retuns result
    
    regex: regular expression  #r"pytumb+.*"
    path: given path
    
    returns: list of found files
    '''
    
    res=re.compile(regex, flags=re.IGNORECASE)
    
    return reduce(add, ([g.group() for g in imap(res.search, files) if g is not None] \
    for tuple_dirs in imap(os.walk, path) for dirs,subdirs,files in tuple_dirs)
    )