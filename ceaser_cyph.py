import string


def fmap():
    #zip(let,xrange(len(let)))
    return sorted(tuple(set((string.letters).lower())))


let = fmap()
sets = tuple(let + list(string.punctuation))


def gmod(Y):
    global let, sets
    Y = list(''.join([i.lower() if i.lower() in sets else ' ' for i in Y]))
    #print Y
    ans = [let[int((let.index(i) + 3) % 26)] if i in let else i for i in Y]
    #print ''.join(ans)
    return ''.join(ans)


def decode(Y):
    global let, sets
    dec_ans = [let[int((let.index(i) - 3) % 26)] if i in let else i for i in Y]
    return ''.join(dec_ans)

if __name__ == "__main__":
    try:
        import sys
        print '\n'
        word = sys.argv[1:]
        ans = ([gmod(w) for w in word])
        print ' '.join(ans)
        print '\n'
        dec_ans = ([decode(w) for w in ans])
        print ' '.join(dec_ans)
    except:
        print 'sys module import fail!'