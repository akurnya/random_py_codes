from decimal import Decimal
D = Decimal
memo = {}


def fib_rec(n):
    if n in memo: return memo[n]
    else:
        if n <= 1:
            temp = memo[n] = 1
            return temp 
        temp = memo[n] = D(n*fib(n-1))
    return temp


#Non recursive
def fib(n):
    for i in xrange(n+1):
        if i in memo: temp = memo[i]
        elif i <= 1:
            temp = memo[i] = 1
        else:
            temp = memo[i] = D(i*temp)
    return temp


if __name__ == "__main__":
    #from cProfile import run
    import time
    from decimal import Decimal
    for i in xrange(100):
        start = time.time()
        fib_rec(i)
        recur = Decimal(time.time()-start)
        start = time.time()
        fib(i)
        nonrecur = Decimal(time.time()-start)
        print "fib non recur {} | fib recur {}".format(nonrecur,recur)
        print "\n"