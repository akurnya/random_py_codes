__author__ = 'Hero'

#import re
#import urllib2
#from urllib2 import urlopen
#import string
import sys
import os
import feedfinder
import feedparser


def rss_parse(urls):

    """

	:param urls:
    Initially you could improvize and build upon this for academic use:
    rsslist1 = []
    rsslist2 = []
    newrss = []
    rss = []
    feedslist = []
    re1='((?:http|https)(?::\\/{2}[\\w]+)(?:[\\/|\\.]?)(?:[^\\s"]*))'   # HTTP URL 1
    pat1 = re.compile(re1,re.IGNORECASE|re.DOTALL)
    re1='.*?'   # Non-greedy match on filler
    re2='((?:http|https)(?::\\/{2}[\\w]+)(?:[\\/|\\.]?)(?:[^\\s"]*))'   # HTTP URL 2
    pat2 = re.compile(re1+re2,re.IGNORECASE|re.DOTALL)
    #urls = [r'http://www.thefreedictionary.com/_/rss-directory.htm', r'http://newsrack
    .in/extras/known-indian-feeds', r'http://dir.yahoo.com/rss/dir/index.php']
    htmllist = [urllib2.urlopen(i).read() for i in urls]
    for html in htmllist:
        t1 = re.findall(pat1, html)
        t2 = re.findall(pat2, html)
        rsslist1 += t1
        rsslist2 += t2
    feeds = rsslist1 + rsslist2
    newrss = [i for i in set(feeds) if i not in newrss]
    for i in newrss:
        if i.endswith('.xml'):
            rss.append(i)
    for i in rss:
        feed = feedparser.parse(i)
        feedslist.append(feed)
    return feedslist
    """
	feedslist = list()
	feeds = [feedfinder.feeds(i) for i in set(urls)]
	for feed in feeds:
		if isinstance(feed, list):
			for i in set(feed):
				fparse = feedparser.parse(i)
				feedslist.append(fparse)
		else:
			fparse = feedparser.parse(feed)
			feedslist.append(fparse)
	return feedslist

if __name__ == "__main__":
	urls = sys.argv[1]
	f = rss_parse(urls)
	path = r'feeds\\'
	if os.path.isdir(path) is False:
		os.mkdir(path)
	for feeds in f:
		with open(path+str(hash(feeds))+'.txt', 'w') as fin:
			fin.write(str(feeds))
			fin.close()