'''
@author: Akul Mathur
Clearly gcd_loop is a winner.

run("gcd_loop(250240000,2987)")
         3 function calls in 0.000 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.000    0.000 <ipython-input-39-54d24f605248>:2(gcd_loop)
        1    0.000    0.000    0.000    0.000 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}



run("gcd_recur(250240000,2987)")
         12 function calls (3 primitive calls) in 0.000 seconds

   Ordered by: standard name

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
     10/1    0.000    0.000    0.000    0.000 <ipython-input-39-54d24f605248>:16(gcd_recur)
        1    0.000    0.000    0.000    0.000 <string>:1(<module>)
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}

'''

#1000 loops, best of 3: 267 µs per loop
def gcd_loop(num,den):
    if num<den: num,den=den,num
    rem = num%den
    #print num,den,rem
    while rem:
        num,den = den,rem
        rem=num%den
        #print num,den,rem
    return num,den,rem


#1000 loops, best of 3: 239 µs per loop
#1000 loops, best of 3: 269 µs per loop
def gcd_recur(num,den):
    if num<den: num,den=den,num
    rem = num%den
    #print num,den,rem
    if rem == 0: return num,den,rem
    num,den = den,rem
    return gcd_recur(num,den)
    #return gcd(den,rem)