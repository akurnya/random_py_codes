"""
Helps in creating user-parser friendly u-r-l like t-h-i-s.
**********************************************************
Borrowed shamelessly from Flask:
Read more: http://flask.pocoo.org/snippets/5/
"""

import re
import unicodedata
from unicodedata import normalize

_punct_re = re.compile(r'[\t !"#$%&\'()*\-/<=>?@\[\\\]^_`{|},.]+')

def slugify(value):
    """
    Normalizes string, converts to lowercase, removes non-alpha characters,
    and converts spaces to hyphens.

    From Django's "django/template/defaultfilters.py".
    """
    if not isinstance(value, unicode):
        value = unicode(value)
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    value = unicode(_punct_re.sub('', value).strip().lower())
    return _punct_re.sub('-', value)