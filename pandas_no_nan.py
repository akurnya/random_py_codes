def pandas_no_nan(d, col):
	#in-place function
	#d = pandas.read_csv(open(f)) #dataframe
	d[col]=d[col].convert_objects(convert_numeric=True)
	d[col]=np.where(np.isnan(d[col]) == True, 0, (d[col]))
	return