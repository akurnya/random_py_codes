from itertools import combinations_with_replacement
from itertools import permutations
from collections import OrderedDict, Counter

#flips = ('h','t')

coin_flips = lambda f, flips: [list(permutations(i,f)) for i in (combinations_with_replacement(flips,f))]

flip_list = lambda f, flips: set(reduce(lambda X,Y: X+Y, coin_flips(f,flips)))

'''
As per http://forums.udacity.com/questions/100050127/how-to-fill-in-the-table-for-the-3-sided-dice-central-limit-theorem?page=1&focusedAnswerId=100050132#100050132
and Udacity c-st101 19A, Central limit Theorem, 11 exercise
'''
sum_of_combinations = lambda flip_times, flips: OrderedDict(Counter([sum(map(int,i)) for i in flip_list(flip_times, flips)]))

if __name__ == "__main__":

	#Fair Coin 4 flips and exactly one Head(anywhere)/Only first flip Head in 4 flips
	len([True for x in (i for i in flip_list(4)) if x.count('h')==1]) * .5**4 / .5**4
	
	#Fair Coin 4 flips and exactly one Head(anywhere)/first flip must be Heads in
	len([True for x in (i for i in flip_list(4)) if x.count('h')==1]) * .5**4 \
	/(len([True for x in (i for i in flip_list(4)) if 'h' in x if list(x).index('h')==0]) * (.5**4))

