def fib(n):
    '''
    0,1,1,2,3,5,8,13
    '''
    f = float
    a,b = 0,1
    while 1:
        if b > n:
            yield None
            break
        a,b = b,a+b
        yield b/f(a)

def fibRatio(n, thresh):
    GOAL = 1.6180339887
    if n <= 1: return
    for num in fib(n):
        if GOAL - thresh<= num <= GOAL + thresh or num == None:
            break;
        print num