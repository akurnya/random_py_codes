#Probability of taking out any coin out of 2
pf = pl = 0.5


phhtf = .5**3 #P(hht|Fair Coin)
phhtl = .9**2 * .1 #P(hht|Loaded Coin)

pthhf = phhtf #P(thh|Fair Coin)
pthhl = .1 * .9**2 #P(thh|Loaded Coin)

pfhht = pf * phhtf #P(Fair,HHT)

plhht = pl * phhtl #P(Loaded,HHT)

pfthh = pf * pthhf #P(Fair,THH)

plthh = pl * pthhl ##P(Loaded,THH)

pf_hht = pf * pfhht/(pf * pfhht + pl * plhht)  #P(F|HHT) Normalized

pf_thh = pf * pfthh/(pf * pfthh + pl * plthh)  #P(F|THH) Normalized

pl_hht = 1-pf_hht  #P(L|HHT)

pl_thh = 1-pf_thh #P(L|THH)

pl_hht * pl_thh + pf_hht * pf_thh #Drawing same coins & diff/ combination out of sets {FF,FL,LF,LL}