from random import randrange

def factors(s):
    '''
    s: just factorization
    '''
    s_int = 0
    n = s
    factors = []
    i = 2
    while i < n:
        while 1:
            s_int = s/i
            if s - i*s_int: break;
            if s_int == 1: 
                factors.append(i)
                return set(factors)
            s = s_int
            factors.append(i)
        i += 1
    return set(factors)


def rabin_miller(n, target=128):
    """
    Do this for all Odd Prime Suspects.
    Doesn't work for even numbers.
    returns True if prob(`n` is composite) <= 2**(-`target`)"""
    def try_composite(a):
        if pow(a, d, n) == 1:
            return False
        for i in range(s):
            if pow(a, 2**i * d, n) == n-1:
                return False
        return True
    if n==2: return True
    if n%2==0: return False
    s = d = temp = 0
    p = n-1
    factor_list = sorted(factors(p))
    if len(factor_list) == 0: return True
    if len(factor_list) > 2:
        s,d = factor_list[0], reduce(lambda X,Y:X*Y, factor_list[1:])
        assert (2**s * d == p)
    else:
        s,d = factor_list
    while target:
        a = randrange(2,n)
        if try_composite(a): return False
        target -= 1
    return True