from operator import add

def quartile_filtered(data):
    '''
    # remove outliers
    # extract data between lower and upper quartile
    '''
    x = sorted(data)
    ln = len(x)
    #Q1_index = int(round(0.25 * (ln+1)))
    Q1_index = (ln-3)/4
    #Q1 = x[Q1_index]
    #Q2 = med = mean(x[ln/2 - 1], x[ln/2]) if ln%2==0 else x[ln/2]
    #Q3_index = int(round(0.75*(ln+1)))
    Q3_index = (Q1_index * 3) + 3
    #Q3 = x[Q3_index]
    filtered_data = x[Q1_index:Q3_index]
    #assert filtered_data == [x[i] for i in xrange(Q1_index,Q3_index)]
    return filtered_data

#=======Old approaches to calculating quartile=======#
def quartile(data, sort = 0, n = 2):
    '''
    Find Lower, Median and Upper Quartile
    n: number of steps to hop in 4n + 3
    '''
    l = []
    sw = 1
    new = ctr = 0
    x = sorted(data) if sort else data
    ln = len(x)
    while ctr < ln:
        if sw == 1:
            new = ctr + n
            l.append(x[ctr:new])
            sw = 2
        else:
            new = ctr + 1
            l.append(x[ctr:new])
            sw = 1
        ctr = new
    #print l
    l_ln = len(l)
    Q1 = l[:l_ln/2]
    lower_quart = Q1[len(Q1)/2]
    Q2 = l[l_ln/2 + 1:]
    upper_quart = Q2[len(Q2)/2]
    med = l[len(l)/2]
    
    #upper_index = x[ln/2 + 1:]
    #lower_index = x[:ln/2]
    #middle = x[ln/2 - len(lower_index)/2 -1 :ln/2 + len(upper_index)/2 + 2]
    middle = reduce(add, Q1[len(Q1)/2 + 1:]+[l[l_ln/2]]+Q2[:len(Q2)/2])
    
    #if len(middle) == len(data):
    #    middle = x[ln/2 - len(lower_index)/2:ln/2 + len(upper_index)/2 + 1]
    #print middle
    mn = sum(middle)/float(len(middle))
    q1 = np.mean(reduce(add, Q1))
    q2 = np.mean(reduce(add, Q2))
    IQR = q2 - q1
    low_fence = q1 - 1.5*IQR
    up_fence = q2 + 1.5*IQR
    return lower_quart, med, upper_quart, q1, mn,q2, low_fence, up_fence


def quartile2(data, sort = 0, n = 2):
    '''
    Find Lower, Median and Upper Quartile
    n: number of steps to hop in 4n + 3
    '''
    l = []
    new = ctr = 0
    x = sorted(data) if sort else data
    ln = len(x)
    while ctr < ln:
        new = ctr + n
        l.append(x[ctr:new])
        ctr = new
    return l