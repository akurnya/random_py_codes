from itertools import izip, imap
from operator import add
import numpy as np

from Crypto.Cipher import AES

ASCII_BITS = 8

XOR = lambda k,m: k ^ m

#Converts byte string array to int to ascii character
bits_to_string = lambda bitstr: ''.join([chr(int(''.join(np.array(bitstr[i:i+ASCII_BITS], dtype=str)), 2)) for i in xrange(0,bitstr.size, ASCII_BITS)])

#Converts a string to an array of ascii bits
#converts a string to list of binary repr of char
mbin = lambda word: imap(np.binary_repr,imap(ord, word))
#concatenates list of N ASCII_BIT binary repr of char to one string
string_to_bits = lambda word: reduce(add,(map(int,i.zfill(ASCII_BITS)) for i in mbin(word)))


pad_bits_append = lambda small, size: small + [0] * max(0, size - len(small))
pad_bits_array = lambda block, key: np.concatenate((block,np.array([0]*abs(len(block) - len(key)),dtype=int)))

def aes_encoder(block, key):
    block = pad_bits_array(block, key)
    # the pycrypto library expects the key and block in 8 bit ascii 
    # encoded strings so we have to convert from the bit string
    #print block
    assert len(block) == len(key)
    block = bits_to_string(block)
    #print block
    key = bits_to_string(key)
    ecb = AES.new(key, AES.MODE_ECB)
    return string_to_bits(ecb.encrypt(block))


def encrypt_cal(pmsg, key, inject, block_size, block_enc):
    ln = min((len(pmsg),len(inject)))
    #return np.array(block_enc([p^j for p,j in izip(pmsg,inject)],key), dtype=int)
    return np.array(block_enc(XOR(pmsg[:ln],inject[:ln]),key), dtype=int)

def cipher_block_chaining(plaintext, key, init_vec, block_size, block_enc):
    cipher = []
    key = np.array(key, dtype=int)
    pmsg = np.array(plaintext, dtype=int)
    iv = np.array(pad_bits_append(init_vec, block_size), dtype=int)
    assert iv.size == block_size
    [cipher.append(encrypt_cal(pmsg[i:i+block_size], key, np.array(cipher[num-1], dtype=int), block_size, block_enc)) \
    if num > 0 else \
     cipher.append(encrypt_cal(pmsg[i:i+block_size], key, iv, block_size, block_enc)) \
     for num, i in enumerate(xrange(0,pmsg.size,block_size))]
    
    return np.concatenate(cipher)


def test():
    key = string_to_bits('4h8f.093mJo:*9#$')
    iv = string_to_bits('89JIlkj3$%0lkjdg')
    plaintext = string_to_bits("One if by land; two if by sea")

    cipher = cipher_block_chaining(plaintext, key, iv, 128, aes_encoder)
    assert bits_to_string(cipher) == "\xeaJ\x13t\x00\x1f\xcb\xf8\xd2\x032b\xd0\xb6T\xb2\xb1\x81\xd5h\x97\xa0\xaeogtNi\xfa\x08\xca\x1e"

if __name__ == "__main__":
    test()