from decimal import Decimal

def bisect_sqrt(num):
    res = num/float(2)
    endpt = max(1,num)
    start = 0
    ans = Decimal(res)*Decimal(res)
    while ((ans)) != ((num)):
        if ans > num:
            endpt = res
            res = (start+res)/float(2)
        elif ans < num:
            start = res
            res = (res + endpt)/float(2)
        ans = Decimal(res)*Decimal(res)
    return res

if __name__ == "__main__":
    import sys
    i = int
    print bisect_sqrt(0.01)