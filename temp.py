from decorator import decorator


#from functools import wraps




#Define the Memoized cached functions



def memoize_uw(func):
    func.cache = {}
    def _memoize(func, *args):  #, **kw):
        cache = func.cache # attribute added by memoize
        """
    
        if kw: # frozenset is used to ensure hashability
    
    
            key = args  #, frozenset(kw.iteritems())
    
    
        else:
    
        """
    
        key = args
    
    
        if key in cache:
    
    
            return cache[key]
    
    
        else:
    
    
            cache[key] = result = func(*args)  #, **kw)
    
    
            return cache
            
    return decorator(_memoize, func)




@memoize_uw


def fact(n):


    if n <= 1: return 1


    return n*fact(n-1)