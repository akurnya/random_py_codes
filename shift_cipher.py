from itertools import izip
from collections import deque

xor = lambda a,b: a^b #a+b % 2


#Given map_dict contains alphabet -> number and 
#number -> alphabet mapping
def shift_cipher_letters_encode(lett, map_dict, a,b):
    y = (map_dict[lett] * a + b) % 26
    return y

def shift_cipher_letters_decode(y, map_dict, a,b):
    x = map_dict[(y-b)*a**-1 % 26]
    return x

def stream_cipher_encode(x,s, enc_func):
    y = xor(x,enc_func(s))  #(x+s) % 2
    return y
    
def stream_ciher_decode(y,s, dec_func):
    #dec_func usually same as enc_func
    x = xor(y,dec_func(s))  #(y + s) %2
    return x

'''
#linear shift register FlipFlop in a more generic sense: 
#each output * multiplier P = {0,1}
#s = [0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0]
#p = [1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0]
'''
series = lambda S,P: reduce(xor, (Si*Pi for Si,Pi in izip(S,P)))


def next_s_generic(S,P):
    '''
    lists of 1s and 0s:
    S: deque
    P: simple list of multipliers
    Again a Simple Linear Shift Reg.
    From L-R
    Steps:
    1. First a set of state S conditions initiate over D FF.
    2. Pass that two this function with states of multipliers P;
    3. Compute them using Si = Si-1 * Pi-1 and xor over each passing result
    4. Store that result to the left of FF-deque S here and pop the right most result. 
    this is the output string Cipher that comes out.
    5. Send another set of P states - multipliers.
    6. Repeat from Step 2.
    '''
    while 1:
        res = series(S,P)  #S*P
        output_str = S.pop()
        yield res, output_str
        S.appendleft(res)
        #S = yield S
        P = yield

#An example of how to use next_s_generic
def gen_8_bits(S,P8):
    '''
    Given 
    p =
    [1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0]
    P = [p for _ in xrange(8)]
        
    gen_8_bits(d, P)
    Out[257]: [1, 1, 1, 0, 1, 0, 0, 1]
    
    gen_8_bits(deque([1, 1, 1, 0, 1, 0, 0, 1]), P)
    Out[258]: [1, 0, 0, 1, 0, 1, 1, 1]
    
    gen_8_bits(deque([1, 0, 0, 1, 0, 1, 1, 1]), P)
    Out[259]: [1, 1, 1, 0, 1, 0, 0, 1]
    
    gen_8_bits(deque([1, 1, 1, 0, 1, 0, 0, 1]), P)
    Out[260]: [1, 0, 0, 1, 0, 1, 1, 1]
    
    Shows that without P being PRG each iteration,
    repeating WILL OCCUR
    ------------------
    This is better:
    gen_8_bits(deque([randint(0,2) for _ in xrange(8)]), P)
    Out[265]: [1, 1, 1, 1, 0, 1, 1, 1]
    
    gen_8_bits(deque([randint(0,2) for _ in xrange(8)]), P)
    Out[266]: [0, 0, 1, 1, 0, 0, 0, 0]
    
    gen_8_bits(deque([randint(0,2) for _ in xrange(8)]), P)
    Out[267]: [0, 0, 1, 0, 0, 1, 0, 0]
    
    gen_8_bits(deque([randint(0,2) for _ in xrange(8)]), P)
    Out[268]: [1, 0, 1, 0, 0, 0, 1, 0]
    
    gen_8_bits(deque([randint(0,2) for _ in xrange(8)]), P)
    Out[269]: [0, 0, 1, 1, 0, 0, 0, 1]
    
    gen_8_bits(deque([randint(0,2) for _ in xrange(8)]), P)
    Out[270]: [0, 1, 0, 0, 0, 0, 1, 0]
    '''
    outs = []
    res = next_s_generic(S,P[0])
    outs.append(next(res)[1])
    for p in P8[1:]:
        res.send(None)
        outs.append(res.send(p)[1])
    return outs