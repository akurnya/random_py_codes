import math, sys, cProfile
import multiprocessing

#new = multiprocessing.Manager().list()
#multiples = multiprocessing.Manager().list()
#pool = multiprocessing.Pool()
"""
def prime_mapper(j):
    global new, multiples
    new.append(j)
    if int(math.sqrt(j)) == math.sqrt(j) or j%2 == 0 or j%3 == 0 or j%5 == 0 and j!=2 and j!=3 and j != 5:
        multiples.append(j)
"""

def optimized_sieve(n):
    multiples = []
    #l = []
    #it = 2
    j = 2
    new = []
    k = n/2
    #q = Queue.Queue()
    #new = map(it, range(2,n/2))
    while j <= k:
        #q.put(i)
        new.append(j)
        """
        if int(math.sqrt(j)) == math.sqrt(j) or j%2 == 0 or j%3 == 0 or j%5 == 0 and j!=2 and j!=3 and j != 5:
            multiples.append(j)
        """
    #iterate = copy.deepcopy(new)
    multiples.extend([j for j in new if int(math.sqrt(j)) == math.sqrt(j) or j%2 == 0 or j%3 == 0 or j%5 == 0 and j!=2 and j!=3 and j != 5])
    #l.extend([j for i in new for j in new if j%i!=0])
    #print set(l), set(multiples)
    primes = [i for i in set(new)-set(multiples) if n%i==0]
    return tuple(sorted(primes))

if __name__ == "__main__":
    n = int(sys.argv[1])
    #k = n/2
    p = Process(target=optimized_sieve, args=(n,))
    p.start()
    p.join()
    """
    for j in xrange(2,k+1):
        pool.apply_async(prime_mapper, (j,))
    pool.close()
    pool.join()
    """
    #print new
    #print '==================\n'
    #print multiples
    #prime = optimized_sieve(n)
    #p = [i for i in prime for x in xrange(2,(i/2)+1) if i%x != 0]
    #print prime
    #divisible_prime = [i for i in prime if n%i==0]
    #print divisible_prime
    #cProfile.run('optimized_sieve(n)')