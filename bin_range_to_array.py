from itertools import imap
import numpy as np

binrange = lambda n: np.array(list({tuple(i.zfill(n)) for i in \
imap(np.binary_repr, xrange(2**n))}),dtype=int)