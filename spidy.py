import spidermonkey
js = spidermonkey.Runtime()
js_ctx = js.new_context()
script = """
        $("#signup-form").submit(function(){ 
        	//check the form is not currently submitting  
    		if($(this).data('formstatus') !== 'submitting'){  
    	  
    			 //setup variables  
    			 var form = $(this),  
    			 formData = form.serialize(),  
    			 formUrl = form.attr('action'),  
    			 formMethod = form.attr('method'), 
    			 responseControl = $('#signup-response-control'),
    			 alertbox = $('#alert-box'),
    			 responseMsg = $('#signup-response');
    			 
    			 //add status data to form  
    			 form.data('formstatus','submitting');  
    	  
    			 //show response message - waiting  
    			 responseControl.hide();
    			 responseControl.fadeIn(200);
    			 
    			 responseMsg.removeClass('response-error'); 
    			 responseMsg.removeClass('response-success'); 
    			 
    			 alertbox.removeClass('alert-error');
    			 alertbox.removeClass('alert-success');
    			 
    			 responseMsg.hide()  
    						.addClass('response-waiting')  
    						.text('Please Wait...')  
    						.fadeIn(200);  
    	  
    			 //send data to server for validation  
    			 $.ajax({  
    				 url: formUrl,  
    				 type: formMethod,  
    				 data: formData,  
    				 success:function(data){  
    	  
    					//setup variables  
    					var responseData = jQuery.parseJSON(data),   
    						klass = '', alertclass='';  
    	  
    					//response conditional  
    					switch(responseData.status){  
    						case 'error':  
    							klass = 'response-error';  
    							alertclass ='alert-error';
    						break;  
    						case 'success':  
    							klass = 'response-success';  
    							alertclass ='alert-success';
    						break;    
    					}  
    	  
    					//show reponse message  
    					responseMsg.fadeOut(200,function(){  
    						alertbox.addClass(alertclass);
    					   $(this).removeClass('response-waiting')  
    							  .addClass(klass)  
    							  .text(responseData.message)  
    							  .fadeIn(200,function(){  
    								  //set timeout to hide response message  
    								  // setTimeout(function(){  
    									  // responseMsg.fadeOut(200,function(){  
    											// $(this).removeClass(klass); 
    											// alertbox.addClass(alertclass);										  
    											// form.data('formstatus','idle');  
    									  // });  
    									  // responseControl.hide();
    								   // },5000); 
    								   
    								   // if Not Hiding after some time
    								   alertbox.addClass(alertclass);
    								   form.data('formstatus','idle');  
    							   });  
    					});  
    					if (responseData.status == 'success') {
    						$('.signup-group').fadeOut(200,function(){
    							$('.verify-group').fadeIn(200);
    						});
    					}
    			   }  
    		  });  
    		}  
      
    		//prevent form from submitting  
    		return false;  
          
        });  
    	
    	$('#verify-button').click(function() {
    		//check the form is not currently submitting  
    		if($('#signup-form').data('formstatus') !== 'submitting'){  
    			var formUrl = 'http://216.51.232.217/teamb/eb9framework/tribes/accounts/verify/';
    			var formMethod = 'post';
    			var formData = $('#inputActivationCode').val();
    			var responseControl = $('#signup-response-control'),
    				 alertbox = $('#alert-box'),
    				 responseMsg = $('#signup-response'),
    				 form = $('#signup-form');
    				 
    			//add status data to form  
    			form.data('formstatus','submitting');  
    		  
    			
    			// show response message - waiting  
    			responseControl.hide();
    			responseControl.fadeIn(200);
    
    			responseMsg.removeClass('response-error'); 
    			responseMsg.removeClass('response-success'); 
    
    			alertbox.removeClass('alert-error');
    			alertbox.removeClass('alert-success');
    
    			responseMsg.hide()  
    				.addClass('response-waiting')  
    				.text('Please Wait...')  
    				.fadeIn(200);  
    			
    			// send data to server for validation  
    			$.ajax({  
    				url: formUrl,  
    				type: formMethod,  
    				data: {inputActivationCode: formData},  
    				success:function(data){  
    					//alert(data);  // to check response
    					
    					// setup variables  
    					var responseData = jQuery.parseJSON(data),   
    					klass = '', alertclass='';  
    
    					// response conditional  
    					switch(responseData.status){  
    						case 'error':  
    							klass = 'response-error';  
    							alertclass ='alert-error';
    							break;  
    						case 'success':  
    							klass = 'response-success';  
    							alertclass ='alert-success';
    							break;    
    					}  
    
    					// show reponse message  
    					responseMsg.fadeOut(200,function(){  
    						alertbox.addClass(alertclass);
    						$(this).removeClass('response-waiting')  
    							.addClass(klass)  
    							.text(responseData.message)  
    							.fadeIn(200,function(){ 
    								alertbox.addClass(alertclass);
    								form.data('formstatus','idle');  
    							});  
    					});  
    					if (responseData.status == 'success') {
    						setTimeout(function(){ window.location = 'http://216.51.232.217/teamb/eb9framework/tribes/accounts/login/'; }, 3000);
    					}
    				}  
    			});  
    		}
    		
    		//prevent form from submitting  
    		return false;  
    	});"""
r = js_ctx.execute(script)
print r