"""
Logical Steps
1. Move the top N - 1 disks from Src to Aux (using Dst as an intermediary peg)
2. Move (1) the bottom disk from Src to Dst
3. Move N - 1 disks from Aux to Dst (using Src as an intermediary peg)
_______________________________________________________________
F(N-1) + 1 + F(N-1) = 2F(N-1) + 1 ........Logical Steps

From my Iterative Analysis per Step N = 1...n =>
F(N-1) + 2**N-1 .......... Iterative Analysis

Equating Eqns:

2F(N-1) + 1 = F(N-1) + 2**N-1

Or F(N-1) = 2**N-1 - 1

OR F(N) = 2**N - 1 :-)
"""

#Calculate How many steps?
def cal_hanoi_tower(N):
    return 2**N - 1
    if N < 1: return None
    if N == 1: return 1
    prev = N-1
    return cal_hanoi_tower(prev) + 2**prev


#Iterative Solution
def solve_hanoi_brahma(N, S, H , D):
    if N == 1:
        print 'from '+S+' to '+D
    else:
        solve_hanoi_brahma(N-1, S, D, H)
        solve_hanoi_brahma(1, S, H, D)
        solve_hanoi_brahma(N-1, H, D, S)