#fact = factorial module I've written. Import it
from factorial_memo import fact

#AKS Primalty Test - Thanks Rossetta Code


def fp(p):

    """

    AKS Primalty Test: (x-1)**p - (x**p-1)

    #multiply nCk = fact(n)/fact(n-k)*fact(k) AND (-1)**k 

    #from binomial expansion

    if fp(p): p is Prime Number

    """
    def _coeff_add(i):
        res = ((-1)**i)*factp/(fact(p-i)*fact(i))
        #if i != 0 and i != p: return res
        if i==0: return 0
        if i==p: return res + 1
        return res
    factp = fact(p)
    return not any([_coeff_add(i)%p!=0 for i in xrange(p)])
    #subp=[(-1),1]
    #penult = p+1
    #print "penult {}".format(penult)
    #print "factp {}".format(factp)

    #zero = 1 + subp[0]

    #coeff_list = [_coeff_add(i) for i in xrange(p)]

    #coeff_list[0] += subp[0]

    #coeff_list[-1] += subp[-1]

    #return any([num%p for num in coeff_list])

    #return not any([num%p for num in [_coeff_add(i) for i in xrange(p)]])
    #fn = lambda X:((-1)**X)*factp/(fact(p-X)*fact(X))
    """
    For testing only
    a= [(_coeff_add(i),_coeff_add(i)%p!=0) for i in xrange(p+1)]
    #print a
    x,y = zip(*a)
    #print x
    #print y
    return not any(y)
    """

if __name__ == "__main__":
    print fp(2)
    print fp(3)
    print fp(20)
    print fp(50)
    print fp(7)