from itertools import izip_longest
from GF2 import GF2

def dot(u,v):
    i = None
    l = (izip_longest(u,v))
    res = [(int(i[0])*int(i[-1])) for i in l if None not in i]
    print res
    return GF2(res)

def c(num, size = 4):
    outp = []
    size = pow(2,(size))
    #f = lambda k, j: zip(k, list(j))
    if isinstance(num, str):
        num = list(num)
    elif isinstance(num, int):
        # num = list(str(num))
        raise Exception("%s should be qouted in qoutations. try again" %(num))
    X = (bin(i)[2:].zfill(4) for i in xrange(size))
    for y in X:
        # Scalar multiplication
        res = [(int(i[0]) * int(i[-1])) for i in zip(num, list(y))]
        #print res
        # And then GF addition
        o = GF2(res)
        #print o
        #print y
        if  o == 1:
            #print y
            outp.append(y)
    return outp


def list_of(list_c):
    l = []
    for i in list_c:
        if len(l) != 0:
            for y in c(i):
                if y not in l:
                    pass
                else:
                    continue
        else:
            l += c(i)
    return l