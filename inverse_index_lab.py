from random import randint
from dictutil import *

## Task 1
def movie_review(name):
    """
    Input: the name of a movie
    Output: a string (one of the review options), selected at random using randint
    """
    review_options = ["See it!", "A gem!", "Ideological claptrap!"]
    return review_options[randint(0,len(review_options)-1)]

## Tasks 2 and 3 are in dictutil.py

## Task 4


def makeInverseIndex(strlist):
    """
    Input: a list of documents as strings
    Output: a dictionary that maps each word in any
    document to the set consisting of the document
    ids (ie, the index in the stror word in line.split():list)
    for all documents containing the word.
    Note that to test your function,
    you are welcome to use the files stories_small.txt
      or stories_big.txt included in the download.
    """
    from collections import defaultdict
    dct = defaultdict(set)
    for i in strlist:
        with open(i, 'r') as fin:
            count = 0
            #print fin
            for line in fin.xreadlines():
                try:
                    #print line
                    for word in line.split():
                        print word
                        if len(word) != 0:
                            #print word, type(word),
                            #print int(count)
                            if word in dct and count not in dct[word]:
                                dct[word].update({count})
                                #dct[word] = set([dct.get(word)]).add(set([count]))
                            elif word not in dct:
                                dct[word] = {count}
                            #dct[word] = set(dct[word])
                except (TypeError, NameError) as e:
                    #print 'keyerror!!'
                    pass
                count += 1
            fin.close()
    return dct



## Task 5
def orSearch(inverseIndex, query):
    """
    Input: an inverse index, as created by makeInverseIndex, and a list of words to query
    Output: the set of document ids that contain _any_ of the specified words
    """
    return ...

## Task 6
def andSearch(inverseIndex, query):
    """
    Input: an inverse index, as created by makeInverseIndex, and a list of words to query
    Output: the set of all document ids that contain _all_ of the specified words
    """
    return ...
