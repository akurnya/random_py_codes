# Enter your code here. Read input from STDIN. Print output to STDOUT
I = int
def ap_next(in_val,arr_val):
    if 3<= in_val <= 2500 and len(arr_val) == in_val:
        prev = None
        prev_res = None
        for next_input in arr_val:
            next_input = I(next_input)
            if prev:
                res = next_input - prev
                if prev_res and res != prev_res:
                    print prev + prev_res
                    return prev + prev_res
                prev = next_input
                prev_res = res
            else:
                prev = next_input
        return
    return

if __name__ == "__main__":
    from cProfile import run
    in_val = I(raw_input())
    arr_val = (str(raw_input())).split()
    run("ap_next(in_val,arr_val)")