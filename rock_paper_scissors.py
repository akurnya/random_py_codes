#Rock-paper-scissors-lizard-Spock GAME core!
# Rock-paper-scissors-lizard-Spock template
import random

# The key idea of this program is to equate the strings
# "rock", "paper", "scissors", "lizard", "Spock" to numbers
# as follows:

#REFER TO THIS. I HAVE CHANGED THE ORDER FOR CONVENIENCE AS PER THE WIKI:
# 0 - scissors
# 1 - paper
# 2 - rock
# 3 - lizard
# 4 - spock

# helper functions
data2num = dict([(name,i) for i,name in enumerate(['scissors','paper','rock','lizard','spock'])])
num2name = dict([(data2num[i],i) for i in data2num])

def name_to_number(name):
    # convert name to number using if/elif/else
    # don't forget to return the result!
    return data2num[name]


def number_to_name(number):
    # convert number to a name using if/elif/else
    # don't forget to return the result!
    return num2name[number]
    
    

def rpsls(player_choice): 
    def win(fst,lst):
        """
        Based on Order of items given above:
        if diff = 1, fst, -1 lst
        if diff = 2, fst , -2 lst
        if diff = 3, lst; -3, fst
        if diff = 4, fst; -4, lst
        """
        windct = {1:fst,
                  -1:lst,
                  2:fst,
                  -2:lst,
                  3:lst,
                  -3:fst,
                  4:fst,
                  -4:lst,
                  0:0}
        return windct[fst - lst]
    
    playdct = {}
    # print a blank line to separate consecutive games
    print " "*10
    print "\n"
    
    # print out the message for the player's choice
    player = (raw_input("Player chooses?: ")).lower()
    print "Player chooses %s" %(player)

    # convert the player's choice to player_number using the function name_to_number()
    player_num = name_to_number(player)
    playdct[player_num] = player

    # compute random guess for comp_number using random.randrange()
    comp_number = random.randint(0,4)
    # convert comp_number to comp_choice using the function number_to_name()
    pc_player = number_to_name(comp_number)
    # print out the message for computer's choice
    print "Computer chooses %s" %(pc_player)
    playdct[comp_number] = pc_player
    
    # compute difference of comp_number and player_number modulo five
    res = win(comp_number,player_num)
    # use if/elif/else to determine winner, print winner message
    if res == 0:
        print "It's a Tie!"
    else: print "%s wins!" %(playdct[res])


if __name__ == "__main__":
    # test your code - LEAVE THESE CALLS IN YOUR SUBMITTED CODE
    assert rpsls("rock")
    assert rpsls("Spock")
    assert rpsls("paper")
    assert rpsls("lizard")
    assert rpsls("scissors")

# always remember to check your completed program against the grading rubric