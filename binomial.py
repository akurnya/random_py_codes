
from math import sqrt
from decimal import Decimal

import numpy as np
import matplotlib

def binomial(n,k):
    '''
    Optimized by Crossing off whichever is greater n-k or k 
    and count down only till n...k+1 or n-(k+1)
    '''
    if k>n: return 0
    sub = n-k
    if n - sub == 0 or sub == 0:
        return np.math.factorial(n)/(Decimal(np.math.factorial(sub)*np.math.factorial(k)))
    return reduce(lambda x,y: x*y, xrange(n,sub,-1))/\
    Decimal(np.math.factorial(k)) if sub > k else \
    reduce(lambda x,y: x*y, xrange(n,k,-1))/Decimal(np.math.factorial(sub))



def binomial_var_prob(n,k,p):
    '''
    Binomial Probability Distribution
    ----------------------------------
    Optimized by Crossing off whichever is greater n-k or k 
    and count down only till n...k+1 or n-(k+1)
    des_times: k,how many instance of p in one outcomes?
    #not added below var but can add when number of not p var are more than 1 State;
    inv_des_state: undesired times, the flip state and how many times
    usually inv_des_state is 1-des_times;
    des_times + inv_des_state == n
	binomial(n,k) * p^k * (1-p)^(1-k) s.t. k + 1 - k = 1 naturally
    '''
    return binomial(n,k) * Decimal((p**k) * (1-p)**(n-k))


def plot_prob_distribution_central_limit(N,p):
    d = ([binomial_var_prob(N,k,p) for k in xrange(N)])
    return matplotlib.pylab.plot(xrange(len(d)), d)

def normal_distro(x,m,dvar):
    #Normal Distribution
    return np.math.e**(-.5 * (x-m)**2/float(dvar))/sqrt(2*np.math.pi*(dvar**2))

def change_in_stats(a,b,mu,var):
    '''
    change in new mean and variance given a% and b added
    '''
    return a*mu + b, a**2 * var

def iter_reduce_sum(gen):
    prev = Decimal(0)
    for g in gen:
        prev = g+prev
    return prev
         
        
def polynomial_combinatorial(x,y,n):
    return iter_reduce_sum((binomial(n,k) * x**(n-k) * y**k for k in xrange(n+1)))

def confidence_interval(N,**kwarg):
    '''
    var, mu, p
    '''
    if 'var' in kwarg: var = N**-1 * kwarg['var']
    elif 'p' in kwarg: 
        p = kwarg['p']
    elif 'mu' in kwarg:
        p = mu = kwarg['mu']
    var = N**-1 * p * (1-p)
    sd = sqrt(var)
    ci = 1.96 * sd
    if 'mu' in kwarg: return (N*kwarg['mu'], ci)
    else: return ci


def critical_region(N,p, critical_val = 5):
    '''
    returns: 
    position in list(total length), value where critical_val% critical value is reached
        '''
    def sum_if(gen, critical_val):
        res = 0.
        num = 0
        prev = 0.
        for i in list(gen):
            res += i
            #print res
            num += 1
            if res*100 > critical_val: return num-1, prev
            prev = res
    gen = [float(binomial_var_prob(N,k,p)) for k in xrange(N+1)]
    #max_a = gen.index(max(gen))
    #l,r = gen[:max_a], gen[max_a+1:][::-1]
    #res = max((l,r), key = lambda x: sum_if(x,critical_val)[1])
    #pos,val = sum_if(gen, critical_val)
    #pos = len(gen) -pos
    return sum_if(gen, critical_val)


def critical_region_2tail(N,p, critical_val = 5):
    '''
    returns: 
    position in list, total value where critical_val% critical value is reached
        '''
    def sum_if(gen, critical_val):
        res = 0.
        num = 0
        prev = 0.
        for i in list(gen):
            res += i
            #print res
            num += 1
            if res*100 > critical_val: return num-1-1, prev
            prev = res
    gen = [float(binomial_var_prob(N,k,p)) for k in xrange(N+1)]
    max_a = gen.index(max(gen))
    l,r = gen[:max_a], gen[max_a+1:][::-1]
    ls,rs = sum_if(l, critical_val/2.), sum_if(r, critical_val/2.)
    #print ls,rs
    pos,val = rs
    pos = len(r) -pos
    return ls,l[ls[0]], len(l) + pos, val, gen[len(l) + 1 + pos]

def hypothesis(N,p, sample, lst, critical_func, critical_val = 5):
    '''
	critical_func: Either critical_region or critical_region_2tail
	'''
    res = critical_func(N,p, critical_val = 5)
    return True if sample in lst[:res] else False

def hypothesis_test(l, h):
    m = mean(l)
    ci = conf(l)
    return m-ci <= h <= m + ci


def linear_reg(xdata,ydata,x_mu,y_mu):
	'''
	b = summation{(x-xmu)*(y-ymu)}/summation((x-xmu)**2)
	'''
    res = Decimal(0)
    bottom = Decimal(0)
    for x,y in izip(xdata,ydata):
        X=Decimal((x-x_mu))
        Y = Decimal((y-y_mu))
        res += (X*Y)
        bottom += (X**2)
    return res/bottom

# ym = b*xm + a => a = ym - b * xm
a_reg = lambda b,xm,ym: ym - b*xm


if __name__ == "__main__":
	'''
	Given a Coin, there are S states, S = 2.
	Now number of tries/flips = 5
	We have P(H) = 0.5
	We want to find out how many possible outcomes have the P(Total Heads) = 1
	'''
	#number of ways to get desired configuration - 1 Head in this case
	#how many possibilities - user binomial(total number,desired outcome)
	#binomial(5,1)

	#Total possibilities for flipping the coin, given State S = 2
	#2**5

	#P(#HEADS) = binomial(5,1)/2**5