from decorator import decorator
import time
#from functools import wraps

def _memoize(func, *args, **kw):
    cache = func.cache # attribute added by memoize
    if kw: # frozenset is used to ensure hashability
        key = args, frozenset(kw.iteritems())
    else:
        key = args
    if key in cache:
        return cache[key]
    else:
        cache[key] = result = func(*args, **kw)
        func.cache.update(cache)
        return result

def memoize_uw(func):
    func.cache = {}
    return decorator(_memoize, func)
    
    
@memoize_uw
def f1(x):
    time.sleep(1) # simulate some long computation
    return x