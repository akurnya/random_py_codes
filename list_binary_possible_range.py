

def list_binary_possible_range(n):
    """
    Self explanatory at what it does:
    poss_val
    array([['0', '0', '0', ..., '0', '0', '0'],
           ['0', '0', '0', ..., '0', '0', '1'],
           ['0', '0', '0', ..., '0', '0', '1'],
           ..., 
           ['0', '1', '1', ..., '1', '1', '1'],
           ['0', '1', '1', ..., '1', '1', '1'],
           ['1', '1', '1', ..., '1', '1', '1']], 
          dtype='|S1')
    """
    zfill_bin = lambda n: (list((binary_repr(i)).zfill(n)) for i in xrange(pow(2,n)))
    return array(list(zfill_bin(n)))