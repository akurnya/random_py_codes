# One time Padding. Akul S
#Trying to crack 2 English word Ciphers by guessing from most commonly used English words

import sys
from string import letters

import numpy as np
from itertools import imap

#MOST COMMON ENGLISH WORDS
W = 'a,able,about,across,after,all,almost,also,am,among,an,and,any,are,as,at,be,because,been,but,by,can,cannot,could,dear,did,do,does,either,else,ever,every,for,from,get,got,had,has,have,he,her,hers,him,his,how,however,i,if,in,into,is,it,its,just,least,let,like,likely,may,me,might,most,must,my,neither,no,nor,not,of,off,often,on,only,or,other,our,own,rather,said,say,says,she,should,since,so,some,than,that,the,their,them,then,there,these,they,this,tis,to,too,twas,us,wants,was,we,were,what,when,where,which,while,who,whom,why,will,with,would,yet,you,your'
W = np.array(W.split(','), dtype = str)

V = 'the, they'
V = np.array(V.split(','), dtype = str)

ASCII_BITS = 7  #ASCII standard padding - 0 prefixes

#XOR = lambda k,m: np.multiply((k+m), (np.invert(k) + np.invert(m)))
XOR = lambda k,m: k ^ m
key2bool = lambda key: np.matrix(key, dtype = bool)
bool2key = lambda k: np.matrix(k, dtype = int)

mbin = lambda word: imap(np.binary_repr,imap(ord, word))

def word2bin(word):
    '''
    Character to Decimal to Binary String
    Bin Str to concatenated Binary matrix array, bool form
    '''
    return np.matrix(np.concatenate([np.matrix(list(i.zfill(ASCII_BITS)),\
    dtype=int) for i in mbin(word)], axis = 1), dtype = int) #mbin[::-1] earlier


def encrypt(k,word):
    #word2bin to bool form
    m = np.matrix(word2bin(word), dtype=bool)
    return XOR(m,k)

#Convert 7 BIT ASCII length matrix numbers(integers) to their ASCII characters
#reverse of word2bin: one word bin-ified matrix from word2bin
def arr_ascii_int2chr(mat):
    mat_word = mat.tolist()[0]
    return (chr(int(''.join(imap(str,mat_word[i:i+7])),2)) \
    for i in xrange(0,mat.size,ASCII_BITS))

def mat_ascii_bin_chops(mat, block_size):
    '''Matrix of ASCII Binary String Chops'''
    yield (mat[:,i:i+block_size] for i in xrange(0,mat.size,block_size))


#One Array Key XOR Cipher Array
def one_by_one_arr_xor(C1, W):
    '''
    XOR Given C1 Cipher and a bunch of keys generated from wordlist W
    Input
    C1: Cipher, serial
    W: Wordlist
    Return: Generator that outputs xor-ed 1D matrix
    '''
    fixd_len = C1.shape[1]
    #Create an array of bit-fied words
    iter_binary_words = imap(word2bin,W)
    for each_bitmat in iter_binary_words:
        r_size = each_bitmat.shape[1]
        rem = fixd_len - r_size
        #make it (1, fixd_len) matrix by zfill
        key_mat = np.concatenate((each_bitmat, np.matrix(list(''.zfill(rem)), dtype=int)), axis = 1)
        #Convert it to number
        number = int(''.join(map(str,key_mat.tolist()[0])),2)
        loop_num = fixd_len/r_size
        #while number > 0:
        print 'Processing %s' %(''.join(list(arr_ascii_int2chr(each_bitmat))))
        while loop_num:
            loop_num -= 1
            out = (bool2key(XOR(key2bool(key_mat), key2bool(C1))))
            yield out
            number >>= ASCII_BITS  #Shift bits ASCII_BITS right OR Divide/(2**ASCII_BITS) in decimal
            key_mat = np.matrix(list((np.binary_repr(number)).zfill(fixd_len)), dtype=int)
        print '-----' *10
    yield 0


#The risky function
def bulk_xor(C1, W):
    c_mat = np.matrix(np.copy(C1))
    #print 'c_mat'
    #print c_mat
    #collect array of all words to matrices, array of matrix of bin strings
    iter_binary_words = imap(word2bin,W)
    #get the len of cipher
    #print iter_binary_words
    fixd_len = C1.shape[1]
    #for each binary word
    for each_bitmat in iter_binary_words:
        #get remaining bits required
        #print 'each_bitmat'
        #print each_bitmat
        r_size = each_bitmat.shape[1]
        rem = fixd_len - r_size
        #make it (1, fixd_len) matrix by zfill
        key_mat = np.concatenate((each_bitmat, np.matrix(list(''.zfill(rem)), dtype=int)), axis = 1)
        #Get that decimal number which is key_mat
        number = int(''.join(map(str,key_mat.tolist()[0])),2)
        loop_num = fixd_len/r_size
        #Build matrix of Keys
        """
        Don't do this! Big Dangerous Memory Killing Loop:
        see my discussion here with Gerret:
        http://stackoverflow.com/a/25389212/2290820
        """
        print 'Processing %s' %(''.join(list(arr_ascii_int2chr(each_bitmat))))
        #while number > 0:
        while loop_num:
            loop_num -= 1
            #Divide integer>> by 2 until it covers all rows
            number >>= ASCII_BITS  #Shift bits ASCII_BITS right OR Divide/(2**ASCII_BITS) in decimal
            key_mat = np.concatenate((key_mat, np.matrix(list((np.binary_repr(number)).zfill(fixd_len)), dtype=int)))
            #Solved! c_mat was increasing so drastically because (c_mat,c_mat) was replacing new c_mat
            c_mat = np.concatenate((c_mat, C1))
            #print key_mat.shape, c_mat.shape
        #XOR(C_MAT, KEY_MAT) BUNCH in one go
        ans = (bool2key(XOR(key2bool(key_mat), key2bool(c_mat))))
        del c_mat
        del number
        print ans.shape
        yield ans
        print '-----' *10
        #Fixed: c_mat too needs to be del and renewed
        c_mat = np.matrix(np.copy(C1))
    yield 0



if __name__ == "__main__":

    C1 = np.matrix(list("1010110010011110011111101110011001101100111010001111011101101011101000110010011000000101001110111010010111100100111101001010000011000001010001001001010000000010101001000011100100010011011011011011010111010011000101010111111110010011010111001001010101110001111101010000001011110100000000010010111001111010110000001101010010110101100010011111111011101101001011111001101111101111000100100001000111101111011011001011110011000100011111100001000101111000011101110101110010010100010111101111110011011011001101110111011101100110010100010001100011001010100110001000111100011011001000010101100001110011000000001110001011101111010100101110101000100100010111011000001111001110000011111111111110010111111000011011001010010011100011100001011001101110110001011101011101111110100001111011011000110001011111111101110110101101101001011110110010111101000111011001111"), dtype = int)

    C2 = np.matrix(list("1011110110100110000001101000010111001000110010000110110001101001111101010000101000110100111010000010011001100100111001101010001001010001000011011001010100001100111011010011111100100101000001001001011001110010010100101011111010001110010010101111110001100010100001110000110001111111001000100001001010100011100100001101010101111000100001111101111110111001000101111111101011001010000100100000001011001001010000101001110101110100001111100001011101100100011000110111110001000100010111110110111010010010011101011111111001011011001010010110100100011001010110110001001000100011011001110111010010010010110100110100000111100001111101111010011000100100110011111011001010101000100000011111010010110111001100011100001111100100110010010001111010111011110110001000111101010110101001110111001110111010011111111010100111000100111001011000111101111101100111011001111"), dtype = int)

    res = bulk_xor(C1, W)
    while 1:
        try:
            t = next(res)
            for i in t:
                print ''.join(list(arr_ascii_int2chr(i)))
        except:
            sys.exit(0)

    '''
    res = one_by_one_arr_xor(C1, V)
    while 1:
        try:
            t = next(res)
            #print t
            let = ''.join(list(arr_ascii_int2chr(t)))
            #if all([not (i not in letters and i not in ' ' and i not in '.' and i not in ',') for i in list(let)]): print let
            print let
            if 0: break;
        except:
            break;
    sys.exit(0)
    '''